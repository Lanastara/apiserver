use std::{
    pin::Pin,
    task::{Context, Poll},
};

use actix_web::{
    dev::ServiceRequest,
    dev::{Payload, Service, ServiceResponse, Transform},
    web, Error, FromRequest,
};
use futures::future::{ok, Ready};
use futures::Future;

use crate::Schemas;

pub struct SchemaValidator {
    schemas: Schemas,
}

impl SchemaValidator {
    fn new(schemas: Schemas) -> Self {
        Self { schemas }
    }
}

impl<S, B> Transform<S> for SchemaValidator
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
    B: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type InitError = ();
    type Transform = SchemaValidatorMiddleware<S>;
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        let schemas = self.schemas.clone();
        ok(SchemaValidatorMiddleware { service, schemas })
    }
}

pub struct SchemaValidatorMiddleware<S> {
    service: S,
    schemas: Schemas,
}

impl<S, B> Service for SchemaValidatorMiddleware<S>
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
    B: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    fn call(&mut self, req: ServiceRequest) -> Self::Future {
        let (r, mut pa) = req.into_parts();

        let path = web::Path::<(String, String)>::from_request(&r, &mut pa)
            .into_inner()
            .unwrap()
            .into_inner();

        let req = ServiceRequest::from_parts(r, pa).ok().unwrap();

        let schema = self.schemas.get_one(&format!("{}/{}", path.0, path.1));

        let fut = self.service.call(req);

        Box::pin(async move {
            let res = fut.await?;

            println!("Hi from response");
            Ok(res)
        })
    }
}
