use serde::Deserialize;
use std::{collections::HashMap, path::PathBuf};

#[derive(Debug, Deserialize)]
#[serde(transparent)]
pub struct App {
    pub apis: HashMap<String, Api>,
}

impl App{
    pub fn get_resource(&self, api: &String, resource: &String) -> Option<&Resource>
    {
        self.apis.get(api)?.get_resource(resource)
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all="PascalCase")]
pub struct Api {
    pub policies: PathBuf,
    pub resources: HashMap<String, Resource>,
}

impl Api{
    pub fn get_resource(&self, resource: &String) -> Option<&Resource>
    {
        self.resources.get(resource)
    }
}

#[derive(Debug, Deserialize, Hash, Eq, PartialEq)]
#[serde(rename_all="PascalCase")]
pub struct Resource {
    pub schema: PathBuf,
    pub data_source: String,
}
