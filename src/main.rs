mod backends;
mod config;
mod middlewares;

use std::{
    collections::hash_map::RandomState,
    collections::HashMap,
    path::PathBuf,
    sync::{Arc, RwLock},
};

use actix_web::{
    delete, get, middleware::Logger, post, put, web, App, HttpResponse, HttpServer, Responder,
};
use backends::Backend;
use env_logger::Env;
use evmap::ReadHandle;
use structopt::StructOpt;
use valico::json_schema::schema::Schema;

type Schemas = ReadHandle<String, Schema, (), RandomState>;
type Policies = ReadHandle<String, Policy, (), RandomState>;
type Resources = ReadHandle<String, config::resource::Resource, (), RandomState>;

struct Policy;

#[get("/api/{api}/{resource}")]
async fn list(
    web::Path((api_key, resource_key)): web::Path<(String, String)>,
    resources: web::Data<Resources>,
    backends: web::Data<Arc<RwLock<HashMap<String, Box<dyn Backend>>>>>,
) -> impl Responder {
    let resource = resources.get(&format!("{}/{}", &api_key, &resource_key));

    if let Some(resource) = resource {
        let resource_lock = resource.get_one();
        if let Some(resource) = resource_lock {
            let lock = backends.read().unwrap();

            if let Some(backend) = lock.get(&resource.data_source) {
                if let Some(response) = backend.list(&api_key, &resource_key) {
                    HttpResponse::Ok().json(response)
                } else {
                    HttpResponse::NotFound().finish()
                }
            } else {
                HttpResponse::NotFound().finish()
            }
        } else {
            HttpResponse::NotFound().finish()
        }
    } else {
        HttpResponse::NotFound().finish()
    }
}

#[get("/api/{api}/{resource}/{id}")]
async fn get(
    web::Path((api_key, resource_key, id)): web::Path<(String, String, uuid::Uuid)>,
    resources: web::Data<Resources>,
    backends: web::Data<Arc<RwLock<HashMap<String, Box<dyn Backend>>>>>,
) -> impl Responder {
    let resource = resources.get(&format!("{}/{}", &api_key, &resource_key));

    if let Some(resource) = resource {
        let resource_lock = resource.get_one();
        if let Some(resource) = resource_lock {
            let lock = backends.read().unwrap();

            if let Some(backend) = lock.get(&resource.data_source) {
                if let Some(response) = backend.get(&api_key, &resource_key, &id) {
                    HttpResponse::Ok().json(response)
                } else {
                    HttpResponse::NotFound().finish()
                }
            } else {
                HttpResponse::NotFound().finish()
            }
        } else {
            HttpResponse::NotFound().finish()
        }
    } else {
        HttpResponse::NotFound().finish()
    }
}

#[post("/api/{api}/{resource}")]
async fn create(
    web::Path((api_key, resource_key)): web::Path<(String, String)>,
    data: web::Json<serde_json::Value>,
    resources: web::Data<Resources>,
    backends: web::Data<Arc<RwLock<HashMap<String, Box<dyn Backend>>>>>,
) -> impl Responder {
    let resource = resources.get(&format!("{}/{}", &api_key, &resource_key));

    if let Some(resource) = resource {
        let resource_lock = resource.get_one();
        if let Some(resource) = resource_lock {
            let mut lock = backends.write().unwrap();

            if let Some(backend) = lock.get_mut(&resource.data_source) {
                if let Some(id) = backend.create(&api_key, &resource_key, data.0) {
                    HttpResponse::Ok().json(id)
                } else {
                    HttpResponse::NotFound().finish()
                }
            } else {
                HttpResponse::NotFound().finish()
            }
        } else {
            HttpResponse::NotFound().finish()
        }
    } else {
        HttpResponse::NotFound().finish()
    }
}

#[put("/api/{api}/{resource}/{id}")]
async fn update(
    web::Path((api_key, resource_key, id)): web::Path<(String, String, uuid::Uuid)>,
    data: web::Json<serde_json::Value>,
    resources: web::Data<Resources>,
    backends: web::Data<Arc<RwLock<HashMap<String, Box<dyn Backend>>>>>,
) -> impl Responder {
    let resource = resources.get(&format!("{}/{}", &api_key, &resource_key));

    if let Some(resource) = resource {
        let resource_lock = resource.get_one();
        if let Some(resource) = resource_lock {
            let mut lock = backends.write().unwrap();

            if let Some(backend) = lock.get_mut(&resource.data_source) {
                if let Some(_) = backend.update(&api_key, &resource_key, id, data.0) {
                    HttpResponse::Ok().finish()
                } else {
                    HttpResponse::NotFound().finish()
                }
            } else {
                HttpResponse::NotFound().finish()
            }
        } else {
            HttpResponse::NotFound().finish()
        }
    } else {
        HttpResponse::NotFound().finish()
    }
}

#[delete("/api/{api}/{resource}/{id}")]
async fn delete(
    web::Path((api_key, resource_key, id)): web::Path<(String, String, uuid::Uuid)>,
    resources: web::Data<Resources>,
    backends: web::Data<Arc<RwLock<HashMap<String, Box<dyn Backend>>>>>,
) -> impl Responder {
    let resource = resources.get(&format!("{}/{}", &api_key, &resource_key));

    if let Some(resource) = resource {
        let resource_lock = resource.get_one();
        if let Some(resource) = resource_lock {
            let mut lock = backends.write().unwrap();

            if let Some(backend) = lock.get_mut(&resource.data_source) {
                if let Some(_) = backend.delete(&api_key, &resource_key, &id) {
                    HttpResponse::Ok().finish()
                } else {
                    HttpResponse::NotFound().finish()
                }
            } else {
                HttpResponse::NotFound().finish()
            }
        } else {
            HttpResponse::NotFound().finish()
        }
    } else {
        HttpResponse::NotFound().finish()
    }
}

async fn run(config: config::resource::App) -> std::io::Result<()> {
    let mut backends: HashMap<String, Box<dyn Backend>> = HashMap::new();

    backends.insert(
        "inmemory".to_string(),
        Box::new(backends::inmemory::InMenoryBackend::default()),
    );

    let config = Arc::new(config);

    let backends = Arc::new(RwLock::new(backends));

    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    HttpServer::new(move || {
        App::new()
            .data(config.clone())
            .data(backends.clone())
            .wrap(Logger::default())
            .service(list)
            .service(get)
            .service(create)
            .service(update)
            .service(delete)
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(parse(from_os_str))]
    manifest: PathBuf,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let opt = Opt::from_args();

    let file = std::fs::File::open(opt.manifest)?;

    let config: config::resource::App = serde_json::from_reader(file)?;

    run(config).await
}
