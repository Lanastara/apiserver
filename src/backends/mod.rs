use std::collections::HashMap;

use serde_json::Value;
use uuid::Uuid;

pub mod inmemory;

pub trait Backend: Send + Sync
{
    fn create(&mut self, api:&String, resource: &String, value: Value) -> Option<Uuid>;
    fn list(&self, api:&String, resource: &String) -> Option<&HashMap<uuid::Uuid, Value>>;
    fn get(&self, api:&String, resource: &String, id: &Uuid) -> Option<&Value>;
    fn update(&mut self, api:&String, resource: &String, id: Uuid, value: Value) -> Option<()>;
    fn delete(&mut self, api:&String, resource: &String, id: &Uuid) -> Option<()>;
}