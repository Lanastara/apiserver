use std::collections::HashMap;

use serde_json::Value;

use super::Backend;

#[derive(Debug, Default)]
pub struct InMenoryBackend {
    data: HashMap<String, HashMap<String, HashMap<uuid::Uuid, Value>>>,
}

impl Backend for InMenoryBackend {
    

    fn get(&self, api: &String, resource: &String, id: &uuid::Uuid) -> Option<&Value> {
        let api = self.data.get(api)?;
        let resource = api.get(resource)?;
        resource.get(id)
    }

    fn create(&mut self, api: &String, resource: &String, value: Value) -> Option<uuid::Uuid> {
        let id = uuid::Uuid::new_v4();

        if !self.data.contains_key(api)
        {
            self.data.insert(api.clone(), HashMap::new());
        }

        let api = self.data.get_mut(api)?;

        println!("api found");

        if !api.contains_key(resource)
        {
            api.insert(resource.clone(), HashMap::new());
        }

        let resource = api.get_mut(resource)?;

        println!("resource found");
        println!("id: {}", id);

        if resource.contains_key(&id) {
            return None;
        }

        resource.insert(id, value);

        Some(id)
    }

    fn list(&self, api: &String, resource: &String) -> Option<&HashMap<uuid::Uuid, Value>> {
        let api = self.data.get(api)?;
        api.get(resource)
    }

    fn update(
        &mut self,
        api: &String,
        resource: &String,
        id: uuid::Uuid,
        value: Value,
    ) -> Option<()> {
        let api = self.data.get_mut(api)?;
        let resource = api.get_mut(resource)?;

        resource.insert(id, value);

        Some(())
    }

    fn delete(&mut self, api: &String, resource: &String, id: &uuid::Uuid) -> Option<()> {
        let api = self.data.get_mut(api)?;
        let resource = api.get_mut(resource)?;

        if !resource.contains_key(&id) {
            return None;
        }

        resource.remove(&id);

        Some(())
    }
}
